#!/bin/env python

from pprint import pprint
#import numpy as np # for Floyd-Warshall matrices

#import pandas as pd

# Graph manipulation functions
##############################

def create_graph(directed = True, weighted = False, weight_attribute = None): 
	"""
	create a dictionnary representing a graph and returns it.
	"""
	g = { 'nodes': {}, 'edges': {}, 'directed': directed, 'weighted': weighted, 'weight_attribute': weight_attribute }
	return g

def add_node(g, node_id, attributes = None): 
	"""
	add a node with node_id (node id provided as a string or int) to the graph g.
	attributes on the node can be provided by a dict.
	returns the node n attributes.
	"""
	if node_id not in g['nodes']: # ensure node does not already exist
		if attributes is None: # create empty attributes if not provided
			attributes = {}
		g['nodes'][node_id] = attributes
		g['edges'][node_id] = {} # init outgoing edges
	return g['nodes'][node_id] # return node attributes

def add_edge(g, node_id1, node_id2, attributes = None, node1_attributes = None, node2_attributes = None): 
	# create nodes if they do not exist
	if node_id1 not in g['nodes']: add_node(g, node_id1, node1_attributes) # ensure node1 exists
	if node_id2 not in g['nodes']: add_node(g, node_id2, node2_attributes) # ensure node2 exists
	# add edge(s) only if they do not exist
	if node_id2 not in g['edges'][node_id1]:
		if attributes is None: # create empty attributes if not provided
			attributes = {}
		g['edges'][node_id1][node_id2] = attributes
		if not g['directed']:
			g['edges'][node_id2][node_id1] = g['edges'][node_id1][node_id2] # share the same attributes as n1->n2
	return g['edges'][node_id1][node_id2] # return edge attributes


def read_delim(filename, column_separator='\t', directed=True, weighted=False, weight_attribute=None): 
	"""
	Parse a text file which columns are separated by the specified column_separator and 
	returns a graph.

	line syntax: node_id1	node_id2	att1	att2	att3	...
	"""
	g = create_graph(directed, weighted, weight_attribute)
	with open(filename) as f: 
		# GET COLUMNS NAMES
		tmp = f.readline().rstrip()
		attNames= tmp.split(column_separator)
		# REMOVES FIRST TWO COLUMNS WHICH CORRESPONDS TO THE LABELS OF THE CONNECTED VERTICES
		attNames.pop(0)  # remove first column name (source node not to be in attribute names)
		attNames.pop(0)  # remove second column (target node ...)
		# PROCESS THE REMAINING LINES
		row = f.readline().rstrip()
		while row:
			vals = row.split(column_separator)
			u = vals.pop(0)
			v = vals.pop(0)
			att = {}
			for i in range(len(attNames)):
				att[ attNames[i] ] = vals[i]
			add_edge(g, u, v, att)
			row = f.readline().rstrip() # NEXT LINE
		return g

def save_delim(g, filename, column_separator='\t'):
	# collect attribute names on edges
	attributes = set()
	for u in g['edges']:
		for v in g['edges'][u]:
			for a in g['edges'][u][v]:
				attributes.add(a)
	# header
	line = 'node_id1'+column_separator+'node_id2'+column_separator+column_separator.join(attributes)
	with open(filename, "w") as f:
		# write headder
		f.write(line+'\n')
		# iterate over each source node, target node and attribute name and write their values
		for u in g['edges']:
			for v in g['edges'][u]:
				line = u + column_separator + v
				for a in attributes:
					val = str(g['edges'][u][v][a]) if a in g['edges'][u][v] else ''
					line += column_separator + val
				f.write(line+'\n')


def load_node_info(g, filename, id_column='node_id', attributes=None, column_separator='\t'):
	import pandas as pd
	df = pd.read_csv(filename, sep=column_separator)
	for i, row in df.iterrows():
		n_id = row[id_column]
		if n_id in g['nodes']:
			if attributes is not None:
				for a in attributes:
					g['nodes'][n_id][a] = row[a]
			else:
				for a in df.columns:
					if a!=id_column:
						g['nodes'][n_id][a] = row[a]



##### main/tests #####
if __name__ == "__main__":
	print("Graph module tests")
